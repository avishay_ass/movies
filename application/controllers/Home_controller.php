<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_controller extends CI_Controller {

    /**
     * Home_controller constructor.
     * Loading Home_model first time only
     */
    function __construct() {
        parent::__construct();
        //Load model
        $this->load->model('home_model');
    }


  function index(){
      //Load view
      $this->load->view('home_view');
      //Load pagination
      $this->load->library('pagination');
    }

    /**
 * @param $page - The current page
 * @param $page_size - Number of results per page
 * @param $keyword - String containing search keyword
 * @return array of results or false
 */
    public function getList()
    {
        //parameters for pagination
        $page = intval($this->input->get('page'));
        $page_size = intval($this->input->get('page_size'));
        $keyword = strval($this->input->get('keyword'));

        $movies = array(); //initialize movies array for empty response

        if ($keyword == ""){
            echo json_encode($movies);
        }
        else{
            $movies = $this->home_model->getList($page, $page_size, $keyword);
            header('Content-Type: application/json');
            if(!empty($movies)){
                array_push($movies, sizeof($movies));
                echo json_encode($movies);
            }
            else{
                $movies = array();
                echo json_encode($movies);
            }
        }
    }

    /**
     * @param $page - The current page
     * @param $page_size - Number of results per page
     * @param $keyword - String containing search keyword
     * @return stdClass or false
     */
    public function getRecordById()
    {
        //parameters for pagination
        $id = intval($this->input->get('id'));


        if ($id == 0){
            echo false;
        }
        else{
            $movie = $this->home_model->getRecordById($id);
            header('Content-Type: application/json');
            if($movie){
                echo json_encode($movie);
            } else {
                echo json_encode(false);
            }
        }
    }


    /**
     * Save movie record to DB
     * sends the incoming data to Home_model to insert into DB
     * @return 0 on success, 1 on failure
     */
    public function saveRecord(){
        $incomingData =array(
            'id' => intval($this->input->post('id')),
            'title' => strval($this->input->post('title')),
            'movie_year' => intval($this->input->post('movie_year')),
            'movie_length' => intval($this->input->post('movie_length')),
            'director' => strval($this->input->post('director')),
            'writer' => strval($this->input->post('writer')),
            'description' => strval($this->input->post('description')),
            'imdb_url' => strval($this->input->post('imdb_url')),
            'imdb_rank' => intval($this->input->post('imdb_rank')),
        );
        $ifNotSucsess = $this->home_model->saveRecord($incomingData);
        header('Content-Type: application/json');
        echo $ifNotSucsess ;
    }

    /**
     * Save movie record to DB
     * sends the incoming data to Home_model to insert into DB
     * @return 0 on success, 1 on failure
     */
    public function updateRecord(){

        $incomingData =array(
            'title' => strval($this->input->post('title')),
            'movie_year' => intval($this->input->post('movie_year')),
            'movie_length' => intval($this->input->post('movie_length')),
            'director' => strval($this->input->post('director')),
            'writer' => strval($this->input->post('writer')),
            'description' => strval($this->input->post('description')),
            'imdb_url' => strval($this->input->post('imdb_url')),
            'imdb_rank' => intval($this->input->post('imdb_rank')),
        );

        $id = intval($this->input->post('id'));

        $ifNotSucsess = $this->home_model->updateRecord($incomingData , $id);
        header('Content-Type: application/json');
        echo $ifNotSucsess ;
    }

    /**
     * delete record from DB by id
     */
    public function deleteRecord(){

        $id = intval($this->input->post('id'));
        $ifNotSuccess = $this->home_model->deleteRecord($id);
        header('Content-Type: application/json');
        echo $ifNotSuccess ;

    }

}


