<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home_model extends CI_Model {

    /**
     * Home_model constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $page - The current page
     * @param $page_size - Number of results per page
     * @param $keyword - String containing search keyword
     * @return array of results or false
     */
    public function getList($page, $page_size, $keyword){

        //Binding from DB
        $query = $this->db
            ->select('*')
            ->from('movies')
            ->where("movies.title LIKE '%$keyword%'", NULL, FALSE)
            ->order_by('movies.title')
            ->limit($page_size, $page*$page_size)
            ->get();

        if($query->num_rows() > 0){
            $result = $query->result_array();
            return $result;
        }else{
            return false;
        }
    }


    /* @param $page - The current page
     * @param $page_size - Number of results per page
     * @param $keyword - String containing search keyword
     * @return array of results or false
     */
    public function getRecordById($id){

        //Binding from DB
        $query = $this->db
            ->select('*')
            ->from('movies')
            ->where('movies.id', $id)
            ->get();

        if($query->num_rows() > 0){
            $result = $query->result_array();
            return $result[0];
        }else{
            return false;
        }
    }

    /**
     * insert incoming data to DB
     * @param $incomingData
     * @return 0 on success, 1 on failure
     */
    public function saveRecord($incomingData){
        $this->db->insert('movies', $incomingData);
        if ($this->db->affected_rows() > 0) // affected_rows will return you the number of affected rows, so if you delete one it will return 1.
        {
            return 0;
        }
        else return 1;
    }

    /**
     * insert incoming data to DB
     * @param $incomingData
     * @return 0 on success, 1 on failure
     */
    public function updateRecord($incomingData , $id){

        $this->db->where('id', $id);
        $this->db->update('movies', $incomingData);
        if ($this->db->affected_rows() > 0) // affected_rows will return you the number of affected rows, so if you delete one it will return 1.
        {
            return 0;
        }
        else return 1;
    }

    /**
     * delete record from DB by id
     * @param $id - id to delete
     * @return 0 on success, 1 on failure
     */
    public function deleteRecord($id){
        $this->db->where('id',$id);
        $this->db->delete('movies');
        if ($this->db->affected_rows() > 0) // affected_rows will return you the number of affected rows, so if you delete one it will return 1.
        {
            return 0;
        }
        else return 1;

    }

}