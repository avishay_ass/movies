<!DOCTYPE html>
<html>

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <title>Movies</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/style/table.css">
    <link rel="stylesheet" type="text/css" href="/assets/style/sweetalert.css">

</head>

<body ng-app="moviesApp">
<div class="container">
    <search-movie></search-movie>
    <div ui-view></div>
</div>




<!--<search-movie/>-->


</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.2/angular-ui-router.min.js"></script>
<script type="text/javascript" src="<?= '/assets/js/services/movieServices.js' ?>" ></script>
<script type="text/javascript" src="<?= '/assets/js/app/moviesApp.js' ?>" ></script>
<script type="text/javascript" src="<?= '/assets/js/controllers/moviesController.js'; ?>" ></script>
<script type="text/javascript" src="<?= '/assets/js/controllers/addMovieController.js'; ?>" ></script>
<script type="text/javascript" src="<?= '/assets/js/controllers/updateMovieController.js'; ?>" ></script>
<script type="text/javascript" src="/assets/js/sweetalert.min.js"></script>
</html>
