/**
 * Created by Avishay on 1/2/2017.
 */
var moviesApp = angular.module('moviesApp', ['movieServices', 'ui.router']);

moviesApp.config(['$httpProvider', '$stateProvider', '$locationProvider', function ($httpProvider, $stateProvider, $locationProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.transformRequest = function (data) {
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
    var templatePath = '/assets/partials/';
    $locationProvider.html5Mode(false);

    $stateProvider
        .state('root', {
            url: '',
            abstract: true,
            template: '<div ui-view=""></div>',
            // controller: 'moviesController'
        })
        .state('root.list', {
            url: '/',
            templateUrl: templatePath + 'moviesTable.html',
            controller: 'moviesController'
        })
        .state('root.edit', {
            url: '/edit/:movieId',
            templateUrl: templatePath+ 'addMovie.html',
            controller: 'updateMovieController',
            resolve: {
                movie: ['$stateParams', 'getMovieByIdFunc', function ($stateParams, getMovieByIdFunc) {
                    return getMovieByIdFunc.getMoviesById($stateParams.movieId);
                }]
            }
        })
        .state('root.add', {
            url: '/add',
            templateUrl: templatePath + 'addMovie.html',
            controller: 'addMovieController',
            resolve: {

            }
        });
}
]);


/**DIRECTIVES TEMPLATES**/

/**
 * An HTML Partials
 * Each element contains the next element
 */


moviesApp.directive('searchMovie', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/partials/searchMovie.html'
    };
});

moviesApp.directive('addMovieForm', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/partials/addMovie.html'
    };
});

moviesApp.directive('tableHead', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/partials/tableHead.html'
    };
});

moviesApp.directive('moviesTable', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/partials/moviesTable.html'
    };
});

moviesApp.directive('moviesForm', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/partials/moviesForm.html'
    };
});

