/**
 * Created by Avishay on 1/9/2017.
 */
moviesApp.controller('addMovieController', ['$scope', '$http', 'getMovieFunc', 'saveMovieFunc', 'deleteMovieFunc', 'updateMovieFunc',
    function ($scope, $http, getMovieFunc, saveMovieFunc, deleteMovieFunc, updateMovieFunc){
        console.log('addMovieController');


        /*
         * Initialize variables ans settings
         */
        // $scope.movie = movie;
        $scope.settings = function () {
            $scope.newRecord = {};
            $scope.message = '';
            $scope.newText = "New Record";
        };


        /*
         * Call first run function
         */
        $scope.init = function () {
            $scope.settings();
        };




        /** FUNCTIONS**/
        /**
         * Save new record from user into server DB
         * @param movieInformation - parameters of movie to be store
         */
        $scope.saveRecord = function (movieInformation) {
            debugger;
            saveMovieFunc.saveRecord(movieInformation).then(function (respone) {
                if (respone.data == 0) {
                    $scope.editing = !$scope.editing; // return back to view mode
                    $scope.message = 'Changes saved correctly';
                }
                else {
                    $scope.message = 'An Error occurred';
                }
            });
        };



        $scope.init();



    }]);