

/**CONTROLLER**/

moviesApp.controller('moviesController', ['$scope', '$http', 'getMovieFunc', 'saveMovieFunc', 'deleteMovieFunc', 'updateMovieFunc',
    function ($scope, $http, getMovieFunc, saveMovieFunc, deleteMovieFunc, updateMovieFunc) {

        console.log('moviesController');
        /*
         * Initialize variables ans settings
         */
        // $scope.movie = movie;
        $scope.settings = function () {
            $scope.movies = [];
            $scope.movieTitle = '';
            $scope.uploadDone = 0;
            $scope.movieParams = {};
            $scope.movieParams.page_size = 10;
            //TODO create responsive page number!
            $scope.movieParams.page = 0;
            $scope.numOfMovies = 0;
            $scope.indexToRemove = 0;
            $scope.editing = 0;
            $scope.insertNewRecord = 0;
            $scope.message = '';
            $scope.newText = "New Record";
        };


        /*
         * Call first run function
         */
        $scope.init = function () {
            $scope.settings();
            $scope.getList();
        };


        /**MAIN FUNCTIONS **/

        /**
         * Get list of movies from server
         * @param page
         * @param page_size
         * @param keyword
         */
        $scope.getList = function (params) {
            getMovieFunc.SearchMovies(params).then(function (respone) {
                if (respone.data) {
                    $scope.numOfMovies = respone.data.pop();
                    $scope.movies = respone.data;
                }
            });
        };


        /**
         * Save new record from user into server DB
         * @param movieInformation - parameters of movie to be store
         */
        $scope.saveRecord = function (movieInformation) {
            debugger;
            saveMovieFunc.saveRecord(movieInformation).then(function (respone) {
                if (respone.data == 0) {
                    $scope.editing = !$scope.editing; // return back to view mode
                    $scope.message = 'Changes saved correctly';
                }
                else {
                    $scope.message = 'An Error occurred';
                }
            });
        };


        /**
         * Update new record from user into server DB
         * @param movieInformation - parameters of movie to be store
         */
        $scope.updateRecord = function (movieInformation) {
            updateMovieFunc.updateRecord(movieInformation).then(function (respone) {
                if (respone.data == 0) {
                    $scope.message = 'Changes saved correctly';
                }
                else {
                    $scope.message = 'An Error occurred';
                }
            });
        };


        /**
         * Delete record from DB by user request
         * @param id - id of record to be deleted
         */
        $scope.deleteRecord = function (id, index) {

            swal({ // Alert before delete row
                title: "Are you sure?",
                text: "You will not be able to recover this row!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            }, function () {
                $scope.indexToRemove = index; //restore index inside scope
                deleteMovieFunc.deleteRecord({id: id}).then(function (respone) {
                    if (respone.data == 0) {
                        $scope.movies.splice($scope.indexToRemove, 1); // remove the elements from movies
                    }
                })
            });
        };


        /**
         * control the view of inserting new record to DB
         */
        $scope.editNewRecord = function () {
            $scope.insertNewRecord = !$scope.insertNewRecord;
            ($scope.insertNewRecord) ? $scope.newText = "Close" : $scope.newText = "Add New";
        };


        $scope.init(); // controller initialization
    }]);