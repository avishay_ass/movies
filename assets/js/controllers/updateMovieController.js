/**
 * Created by Avishay on 1/9/2017.
 */
moviesApp.controller('updateMovieController', ['$scope', '$http', 'getMovieFunc', 'saveMovieFunc', 'deleteMovieFunc', 'updateMovieFunc', 'movie',
    function ($scope, $http, getMovieFunc, saveMovieFunc, deleteMovieFunc, updateMovieFunc, movie) {
        console.log('updateMovieController');

        // console.log('movie id', movie);
        /*
         * Initialize variables ans settings
         */
        $scope.newRecord = movie;
        // debugger;
        $scope.settings = function () {
            // $scope.newRecord = {};
            $scope.message = '';
            $scope.newText = "New Record";
        };


        /*
         * Call first run function
         */
        $scope.init = function () {
            $scope.settings();
        };




        /** FUNCTIONS**/

        /**
         * Get list of movies from server
         * @param page
         * @param page_size
         * @param keyword
         */
        $scope.getList = function (params) {
            getMovieFunc.SearchMovies(params).then(function (respone) {
                if (respone.data) {
                    $scope.numOfMovies = respone.data.pop();
                    $scope.movies = respone.data;
                }
            });

        };

        /**
         * Update new record from user into server DB
         * @param movieInformation - parameters of movie to be store
         */
        $scope.updateRecord = function (movieInformation) {
            updateMovieFunc.updateRecord(movieInformation).then(function (respone) {
                if (respone.data == 0) {
                    $scope.message = 'Changes saved correctly';
                }
                else {
                    $scope.message = 'An Error occurred';
                }
            });
        };


        $scope.init();







    }]);