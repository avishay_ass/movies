/**
 * These services are made for create AJAX calls
 * GET, PUT , and DELETE calls are available
 */


var movieServices = angular.module('movieServices', []);


/**
 * GET call
 * @params - request parameters
 */
movieServices.service('getMovieFunc', ['$http',function ($http) {
    return {
        SearchMovies: function (params) {
            return $http({
                method: 'GET',
                url: '/index.php/home_controller/getList',
                params: params
            });
        }
    }
}]);

/**
 * GET call
 * @params - request parameters
 */
movieServices.service('getMovieByIdFunc', ['$http', '$q',function ($http, $q) {
    return {
        getMoviesById: function (id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '/index.php/home_controller/getRecordById',
                params: {
                    id: id
                }
            }).then(function (resp) {
                deferred.resolve(resp.data);
            }).catch(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }
    }
}]);



/**
 * POST call
 * @params - request parameters
 */
movieServices.service('saveMovieFunc', ['$http',function ($http) {
    return {
        saveRecord: function (MovieInformation) {
            return $http({
                method: 'POST',
                url: '/index.php/home_controller/saveRecord',
                data: MovieInformation
            });
        }
    }
}]);

/**
 * UPDATE call
 * @params - request parameters
 */
movieServices.service('updateMovieFunc', ['$http',function ($http) {
    return {
        updateRecord: function (MovieInformation) {
            return $http({
                method: 'POST',
                url: '/index.php/home_controller/updateRecord',
                data: MovieInformation
            });
        }
    }
}]);

/**
 * DELETE call
 * @param - id to delete
 */
movieServices.service('deleteMovieFunc', ['$http',function ($http) {
    return {
        deleteRecord: function (param) {
            return $http({
                method: 'DELETE',
                url: '/index.php/home_controller/deleteRecord',
                data: param
            });
        }
    }
}]);


